<?php

/** CONNECT STYLES AND SCRIPTS START*/
require_once(__DIR__ . '/inc/styles-and-scripts.php');
/** CONNECT STYLES AND SCRIPTS END*/

/** ADD TO HEAD START*/
require_once(__DIR__ . '/inc/add-to-head.php');
/** ADD TO HEAD END*/

/** SHORTCODES START*/
require_once(__DIR__ . '/inc/addons/shortcodes/shortcodes.php');
/** SHORTCODES END*/

/** ALLOW SVG UPLOAD START*/
require_once(__DIR__ . '/inc/addons/allow-svg.php');
/** ALLOW SVG UPLOAD END*/

/** ADD CUSTOM POST TYPE START*/
require_once(__DIR__ . '/inc/addons/add-custom-posts-type.php');
/** ADD CUSTOM POST TYPE END*/

/** ADD CUSTOM TAXONOMY START*/
require_once(__DIR__ . '/inc/addons/add-custom-taxonomy.php');
/** ADD CUSTOM TAXONOMY END*/

/** ALLOW POST DUPLICATE START*/
require_once(__DIR__ . '/inc/addons/allow-posts-duplicate.php');
/** ALLOW POST DUPLICATE END*/


