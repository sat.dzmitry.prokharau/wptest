<?php


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 100 );
function my_theme_enqueue_styles() {
	$parenthandle = 'twentytwentythree';
	$theme        = wp_get_theme();
	wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css'
	);
	wp_enqueue_style( 'common-style', get_template_directory_uri() . '-child/assets/build/css/main.min.css'
	);
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );
function my_theme_enqueue_scripts(){
	wp_enqueue_script( 'common-scripts', get_template_directory_uri() . '-child/assets/build/js/main.js');
}
