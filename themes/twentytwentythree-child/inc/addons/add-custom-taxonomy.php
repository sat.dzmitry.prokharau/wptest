<?php

add_action( 'init', 'create_custom_taxonomies' );
function create_custom_taxonomies() {

	$taxonomies = [
		[
			'name' => 'genres',
			'for_post_type'     => 'film',
			'label' => 'Genres',
			'labels' => [
				'name'              => 'Genre',
				'singular_name'     => 'Genre',
				'search_items'      => 'Search Genre',
				'all_items'         => 'All Genres',
				'view_item '        => 'View Genre',
				'parent_item'       => 'Parent Genre',
				'parent_item_colon' => 'Parent Genre: ',
				'edit_item'         => 'Edit Genre',
				'update_item'       => 'Update Genre',
				'add_new_item'      => 'Add New Genre',
				'new_item_name'     => 'New Genre',
				'menu_name'         => 'Genre',
				'back_to_items'     => '← Back to Genre 1',
			],
			'description'  => 'Description',
			'public'       => true,
			'hierarchical' => true,
			'rewrite'           => true,
			'capabilities'      => array(),
			'meta_box_cb'       => null,
			'show_admin_column' => false,
			'show_in_rest'      => true,
			'rest_base'         => null,
			'_builtin'          => true,
		],
		[
			'name' => 'country',
			'for_post_type'     => 'film',
			'label' => 'Countries',
			'labels' => [
				'name'              => 'Country',
				'singular_name'     => 'Country',
				'search_items'      => 'Search Country',
				'all_items'         => 'All Country',
				'view_item '        => 'View Country',
				'parent_item'       => 'Parent Country',
				'parent_item_colon' => 'Parent Country:',
				'edit_item'         => 'Edit Country',
				'update_item'       => 'Update Country',
				'add_new_item'      => 'Add New Country',
				'new_item_name'     => 'New Country Name',
				'menu_name'         => 'Country',
				'back_to_items'     => '← Back to Country',
			],
			'description'  => 'Description',
			'public'       => true,
			'hierarchical' => true,

			'rewrite'           => true,
			'capabilities'      => array(),
			'meta_box_cb'       => null,
			'show_admin_column' => false,
			'show_in_rest'      => true,
			'rest_base'         => null,
			'_builtin'          => true,
		],
		[
			'name' => 'actor',
			'for_post_type'     => 'film',
			'label' => 'Actors',
			'labels' => [
				'name'              => 'Actor',
				'singular_name'     => 'Actor',
				'search_items'      => 'Search Actor',
				'all_items'         => 'All Actor',
				'view_item '        => 'View Actor',
				'parent_item'       => 'Parent Actor',
				'parent_item_colon' => 'Parent Actor:',
				'edit_item'         => 'Edit Actor',
				'update_item'       => 'Update Actor',
				'add_new_item'      => 'Add New Actor',
				'new_item_name'     => 'New Actor Name',
				'menu_name'         => 'Actor',
				'back_to_items'     => '← Back to Actor',
			],
			'description'  => 'Description',
			'public'       => true,
			'hierarchical' => true,

			'rewrite'           => true,
			'capabilities'      => array(),
			'meta_box_cb'       => null,
			'show_admin_column' => false,
			'show_in_rest'      => true,
			'rest_base'         => null,
			'_builtin'          => true,
		]
	];

	foreach ($taxonomies as $taxonomy) {
		register_taxonomy($taxonomy['name'], [ $taxonomy['for_post_type'] ], $taxonomy);
	}
}
