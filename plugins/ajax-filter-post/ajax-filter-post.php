<?php

/**
 *
 * @wordpress-plugin
 * Plugin Name:       Ajax filter posts
 * Version:           1.0.0
 * Text Domain:       ajax-filter-post
 */


if ( ! defined( 'WPINC' ) ) {
	die;
}

require plugin_dir_path( __FILE__ ) . 'class-ajax-filter-post.php';

function run_ajax_filter_posts() {

	$plugin = new Ajax_Filter_Posts();

}
run_ajax_filter_posts();
